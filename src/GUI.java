import acm.graphics.GCanvas;
import acm.graphics.GImage;
import acm.program.Program;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;

/**
 * Created by Joseph Crouson on 4/22/16.
 */
public class GUI extends Program
{
    private JTextField zipField;
    private JLabel FtempLabel;
    private JLabel CtempLabel;
    private JLabel KtempLabel;
    private JLabel locationLable;
    private JLabel weatherLable;
    private GCanvas  canvas;

    public GUI()
    {
        start();
    }

    public void init()
    {
        JLabel zipLabel = new JLabel("Zip");
        zipField = new JTextField("95677");
        Dimension d = new Dimension(90, 25);
        zipField.setPreferredSize(d);

        JButton goButton = new JButton("Go");

        FtempLabel = new JLabel("32"+ "\u00B0"+"F");
        Font bigText = new Font("Comic Sans MS", Font.BOLD, 65);
        FtempLabel.setFont(bigText);

        CtempLabel = new JLabel("00"+ "\u00B0"+"C");
        Font medText = new Font("Comic Sans MS", Font.BOLD, 15);
        CtempLabel.setFont(medText);

        KtempLabel = new JLabel("273.15"+"K");
        KtempLabel.setFont(medText);

        locationLable = new JLabel("Earth");
        weatherLable = new JLabel("Atmospheric");


        // Make field black
        Color blackField = new Color(0,0,0);
        Color whiteTXT = new Color(255,255,255);
        zipField.setBackground(blackField);
        zipField.setForeground(whiteTXT);

        // Add canvas to window
        canvas = new GCanvas();
        this.add(canvas);

        // Background image
        GImage bg = new GImage("images/background.jpg");
        canvas.add(bg, 0, 0);

        // Add UI elements to canvas
        canvas.add(zipField);
        zipField.setLocation(400/2-(zipField.getWidth()/2),10);

        canvas.add(zipLabel);
        Font zipFont = new Font("Comic Sans MS",Font.BOLD,18);
        zipLabel.setFont(zipFont);
        zipLabel.setSize(zipLabel.getPreferredSize());
        zipLabel.setLocation(400/2-(zipField.getWidth()/2)-zipLabel.getWidth()-5,10);

        canvas.add(goButton);
        goButton.setLocation(400/2-(goButton.getWidth()/2),10+zipField.getHeight());

        GImage weatherImg = new GImage("images/nt_rain.gif");
        canvas.add(weatherImg);
        weatherImg.setLocation(400/2-(weatherImg.getWidth()/2),600/4);

        canvas.add(FtempLabel);
        FtempLabel.setSize(FtempLabel.getPreferredSize());
        FtempLabel.setLocation(400/2-(FtempLabel.getWidth()/2)-(FtempLabel.getWidth()/2)+25,600/3);

        canvas.add(CtempLabel);
        CtempLabel.setLocation(400/2-(FtempLabel.getWidth()/2)-(FtempLabel.getWidth()/2)+5+FtempLabel.getWidth()+25,600/3+30);
        CtempLabel.setPreferredSize(CtempLabel.getPreferredSize());

        canvas.add(KtempLabel);
        KtempLabel.setLocation(400/2-(FtempLabel.getWidth()/2)-(FtempLabel.getWidth()/2)+5+FtempLabel.getWidth()+25,600/3+30+(CtempLabel.getHeight()));
        KtempLabel.setPreferredSize(KtempLabel.getPreferredSize());

        canvas.add(locationLable);
        locationLable.setFont(medText);
        locationLable.setSize(locationLable.getPreferredSize());
        locationLable.setLocation(400/2-(locationLable.getWidth())-50,350);
        canvas.add(weatherLable);
        weatherLable.setLocation(400/2+50,350);
        weatherLable.setFont(medText);
        weatherLable.setSize(weatherLable.getPreferredSize());


        // Change size of window
        this.setSize(400, 600);

        addActionListeners();
        revalidate();
    }

    public static void main(String[] args)
    {
        GUI g = new GUI();
    }

    public void actionPerformed(ActionEvent ae)
    {
        String cmd = ae.getActionCommand();
        if (cmd.equals("Go"))
        {
            Whether w = new Whether(zipField.getText());
            int tempF = (int) Math.round(w.getTempF());
            int tempC = (int) Math.round(w.getTempC());
            int tempK = (int) Math.round(w.Kelvin_temp());

            // Add degree symbol
            FtempLabel.setText("" + tempF + "\u00B0"+"F");
            CtempLabel.setText("" + tempC + "\u00B0"+"C");
            KtempLabel.setText("" + tempK +"K");

            // Set color of text to match temperature using HSB
            //int tempColor = Color.HSBtoRGB((float)tempF, 255, 255);
            //Color t = new Color(tempColor);
            //FtempLabel.setForeground(t);

            // Reset Labels
            FtempLabel.setLocation(400/2-(FtempLabel.getWidth()/2)-(FtempLabel.getWidth()/2)+25,600/3);
            CtempLabel.setLocation(400/2-(FtempLabel.getWidth()/2)-(FtempLabel.getWidth()/2)+5+FtempLabel.getWidth()+25,600/3+30);
            KtempLabel.setLocation(400/2-(FtempLabel.getWidth()/2)-(FtempLabel.getWidth()/2)+5+FtempLabel.getWidth()+25,600/3+30+(CtempLabel.getHeight()));

            // Change size of label to match preferred size
            FtempLabel.setSize(FtempLabel.getPreferredSize());
            Dimension d = new Dimension(90,25);
            KtempLabel.setSize(d);
            CtempLabel.setSize(d);

            //Change Pic
            GImage weatherImgNew = new GImage(w.newImg());
            canvas.add(weatherImgNew);
            weatherImgNew.setLocation(400/2-(weatherImgNew.getWidth()/2),600/4);

            //Change Location and Weather
            weatherLable.setText(w.getWeather());
            weatherLable.setSize(weatherLable.getPreferredSize());
            locationLable.setText(w.getCityState());
            locationLable.setSize(locationLable.getPreferredSize());

        }
    }
}
/*Notes to Self*/
/*
* How to make border invisible
* How to make window not resize
* How to set the window size to the screen size
* How to center Text in a label
*
*
*
*
*
*
*
*
*
*
*
* */