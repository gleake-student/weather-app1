/*
 * Created by Galen on 4/13/2016.
 */
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;


public class Whether {
    private final String APIKEY = "ed29364e1d609911";
    JsonElement jse;

    public Whether(String zip)
    {
        getJson(zip);
    }

    public void getJson(String zip)
    {
        String json = "";

        try
        {
            URL url = new URL("http://api.wunderground.com/api/" + APIKEY + "/conditions/q/" + zip + ".json");
            InputStream is = url.openStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            String line;
            while ((line = rd.readLine()) != null)
            {
                json += line;
            }
            rd.close();
        }
        catch (MalformedURLException mue)
        {
            mue.printStackTrace();
        }
        catch (IOException ioe)
        {
            ioe.printStackTrace();
        }

        JsonParser parser = new JsonParser();
        jse = parser.parse(json);
    }


    public double getTempF(){

        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_f").getAsDouble();

    }

    public String getCityState(){
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("display_location").getAsJsonObject().get("full").getAsString();
    }

    public String getWeather(){
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("weather").getAsString();
    }

    public double getTempC(){
        return jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("temp_c").getAsDouble();
    }

    public double Kelvin_temp(){
        return (getTempC() + 273.15);
    }

    public String newImg(){
        return (jse.getAsJsonObject().get("current_observation").getAsJsonObject().get("icon").getAsString()+".gif");
    }

}
